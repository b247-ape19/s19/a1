/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

      let user1;
      let pass;
      let message1 = alert("Please log in Your Username and password!")
      let user = prompt('Enter your Username');

     
      if (user === ""){
            // this area if the input username is null the alert will pop-up to warning
         alert('the username input should not be empty..');
         user = prompt('Enter your Username');
        }

        if (user == user && user1 == user1) {
          pass = prompt('Enter your Password');

          // this area if the input password is null the alert will pop-up to warning
           if (pass === ""){
             alert('the password input should not be empty..');
              pass = prompt('Enter your Password');
            }

         if(pass == pass){

            // switch condetion inside the if else statement
            // this condition will determine if the user is admin, teacher, student or the role is out of the range
            role = prompt('Enter your Role').toLowerCase();
                {
                    switch (role) {
                        case 'admin':
                            alert("Welcome back to the class portal, admin!");
                             message1 = prompt("type Y if you continue to check user role and type N if Not").toUpperCase();
                            
                                // for input error purpose
                             if (message1 === ""){ 
                                       alert('Your input is null');
                                       message1 = prompt("type Y if you continue to check user role  type N if Not").toUpperCase();
                                } 
                                else if (message1 !== 'Y' && message1 !== 'N') { 
                                       alert('Your input is incorrect');
                                       message1 = prompt("type Y if you continue to check user role  type N if Not").toUpperCase();
                                }

                             // this statement is to check if the role user is admin or not
                             if (message1 === 'Y') {
                                    message1 = prompt("type again your user role").toLowerCase();

                                 if (role == message1){
                                    alert('can you continue to evaluate the student average');   
                                     console.log('Username: ' + user);
                                     console.log('Role:' + role);
                                     console.log('please Type the 4 number!');
                                 } 
                                 else if (role != message1){
                                    alert(role + "! You are not allowed to access this feature!");
                                 }
                              }
                          
                              if (message1 === 'N') {
                                     alert('can you continue to evaluate the student average');
                                     console.log('Username: ' + user);
                                     console.log('Role:' + role);
                                     console.log('please Type the 4 number!');
                                 }
                            break;

                        case 'teacher':

                            alert("Thank you for logging in, teacher!");
                            message1 = prompt("type Y if you continue to check user role  type N if Not").toUpperCase();

                            // for input error purpose
                             if (message1 === ""){ 
                                       alert('Your input is null');
                                       message1 = prompt("type Y if you continue to check user role  type N if Not").toUpperCase();
                                } 
                                else if (message1 !== 'Y' && message1 !== 'N') { 
                                       alert('Your input is incorrect');
                                       message1 = prompt("type Y if you continue to check user role  type N if Not").toUpperCase();
                                }
                                

                              // this statement is to check if the role user is teacher or not
                              if (message1 === 'Y') {
                                   message1 = prompt("type again your user role").toLowerCase();

                                 if (role == message1){
                                    alert('can you continue to evaluate the student average');
                                     console.log('Username: ' + user);
                                     console.log('Role:' + role);
                                     console.log('please Type the 4 number!');
                                 } 
                                 else if (role != message1){
                                    alert(role + "! You are not allowed to access this feature!");
                                 }
                              }

                             if (message1 === 'N') {
                                     alert('can you continue to evaluate the student average');
                                     console.log('Username: ' + user);
                                     console.log('Role:' + role);
                                     console.log('please Type the 4 number!');
                                 }
                            
                            break;

                        case 'student':
                            alert("Welcome to the class portal, student!");
                            break;

                                default:
                                  alert("Role out of range.");
                                  break;
                        }
                }
           }
      }
        

// this function is to know your averages

     let message;

     function average(num1,num2,num3,num4) {
         
               let result  = num1 + num2 + num3 + num4;
               let AverageResult = result / 4;
               let resultNum = Math.round(AverageResult  );
         
        // if else statement 

        if (resultNum <= 74 ) { // this statement is less than and equal 74

            console.log('hello, student, your average is: ' + resultNum + '. The letter equivalent is F');
        }

       else if(resultNum >= 75 && resultNum <= 79 ) { // this statement is greater than or equal 75 and less than or equal 79
    
            console.log('hello, student, your average is: ' + resultNum + '. The letter equivalent is D');
        }
        else if(resultNum >= 80 && resultNum <= 84){ // this statement is greater than or equal 80 and less than or equal 84
        
            console.log('hello, student, your average is: ' + resultNum + '. The letter equivalent is C');
        } 
        else if(resultNum  >= 85 && resultNum  <= 89){ // this statement is greater than or equal 85 and less than or equal 89
            
            console.log('hello, student, your average is: ' + resultNum  + '. The letter equivalent is B');
        }
        else if(resultNum >= 90 && resultNum  <= 95){ // this statement is greater than or equal 90 and less than or equal 95
           
            console.log('hello, student, your average is: ' +resultNum  + '. The letter equivalent is A');
        }
        else if(resultNum >= 96){ // this statement is greater than and equal 96
    
            console.log('hello, student, your average is: ' + resultNum  + '. The letter equivalent is A+');
        }
                         
    };

   average();
    


